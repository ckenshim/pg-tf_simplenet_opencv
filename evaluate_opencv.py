import cv2

def load_model(path):

    cv2.dnn.readNetFromTensorflow(path)


if __name__ == "__main__":

    load_model("./my_model.pb")
