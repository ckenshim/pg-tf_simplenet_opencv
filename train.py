import cv2
import tensorflow as tf
from tensorflow.keras import Sequential, layers
import numpy as np
from helper_tools import freeze_session
from tensorflow.keras import backend as K
import os

def build_model():
    model = Sequential()
    # model.add(layers.Conv2D(16, (3, 3), padding="same", name="conv_1", activation='relu'))
    # model.add(layers.Conv2D(16, (3, 3), padding="same", name="conv_2", activation='relu'))
    # model.add(layers.Conv2D(16, (3, 3), padding="same", name="conv_3", activation='relu'))
    # model.add(layers.Flatten())
    # model.add(layers.Input(shape=(100, 32)))
    # model.add(layers.LSTM(30, return_sequences=True, go_backwards=False))
    # model.add(layers.LSTM(30, return_sequences=True, go_backwards=True))
    # model.add(layers.TimeDistributed(layers.Dense(20)))
    # model.add(layers.Flatten())
    # model.add(layers.Dense(10, name="dense_2", activation='softmax'))


    # inputs = tf.keras.Input(shape=(100, 32), name="input_1")
    # inputs = tf.reshape(inputs, shape=(-1, 100, 32))
    # inner = layers.LSTM(30, return_sequences=True, go_backwards=False)(inputs)
    # inner = layers.LSTM(30, return_sequences=True, go_backwards=True)(inner)
    # inner = layers.TimeDistributed(layers.Dense(20))(inner)
    # inner = layers.Flatten()(inner)
    # preds = layers.Dense(10, name="dense_2", activation='softmax')(inner)
    #
    # model = tf.keras.Model(inputs=inputs, outputs=preds)

    # ************* ************** *****************
    hidden_size = 60
    vocabulary = 10
    num_steps = 3
    model = Sequential()
    model.add(layers.Conv2D(5, kernel_size=(3,3), padding="same"))
    # model.add(layers.Reshape(target_shape=(20, 100)))
    # model.add(layers.LSTM(hidden_size, return_sequences=True))\
    # if use_dropout:
    #     model.add(layers.Dropout(0.5))
    # model.add(layers.TimeDistributed(layers.Dense(vocabulary)))
    model.add(layers.Activation('softmax'))

    return model


def run_training(model, data, labels):
    model.compile(optimizer=tf.train.AdamOptimizer(0.001),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    model.fit(data, labels, epochs=3, batch_size=18)


def get_data():
    data = np.random.random((1000, 20, 20, 3))
    labels = np.random.random((1000, 20, 10))
    return data, labels


def save_whole_model_as_one_bin_protobuf(path, model):
    frozen_graph = freeze_session(K.get_session(),
                                  output_names=[out.op.name for out in model.outputs])

    tf.train.write_graph(frozen_graph, path, "my_model.pb", as_text=False)


if __name__ == "__main__":


    model = build_model()
    data, labels = get_data()
    run_training(model, data, labels)

    os.remove("./my_model.pb")
    save_whole_model_as_one_bin_protobuf(".", model)
